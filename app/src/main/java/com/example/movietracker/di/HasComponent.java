package com.example.movietracker.di;

public interface HasComponent <C> {
    C getComponent();
}
