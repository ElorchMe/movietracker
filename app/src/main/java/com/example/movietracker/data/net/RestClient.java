package com.example.movietracker.data.net;

import com.example.movietracker.data.net.api.MovieApi;

public interface RestClient {
    MovieApi getMovieApi();
}
