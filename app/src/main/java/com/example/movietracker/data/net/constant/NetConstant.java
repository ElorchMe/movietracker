package com.example.movietracker.data.net.constant;

public class NetConstant {
    public static final String API_KEY = "144fd08a3ef3ff0aef7a4ff25cbb980d";
    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    public static final String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w185";

    private NetConstant() {

    }
}
